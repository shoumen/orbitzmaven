package com.orbitz.tc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.orbitz.globalvariables.GlobalVariable;
import com.page.objects.PageObjects;
import com.relevantcodes.extentreports.LogStatus;

public class OrbitzFlow extends  PageObjects  {

	public void click_Flight (WebDriver dr)
	{
		//CmnKeywrd.clickObject(tab_Flight(dr));
		tab_Flight(dr).click();
		GlobalVariable.LOGGER.log(LogStatus.INFO, "Successfully clicked Flight Tab " );
		//GlobalVariable.LOGGER.log(LogStatus.PASS, 	GlobalVariable.LOGGER.addScreenCapture("c:\\Report\\image1.png"));

	}
	public void click_OneWay (WebDriver dr)
	{
		tab_Oneway(dr).click();
		GlobalVariable.LOGGER.log(LogStatus.INFO, "Successfully clicked Oneway Tab " );
	}

	public void setText_Flyfrom (WebDriver dr)
	{
		txtbox_FlightOrigin(dr).sendKeys("Mumbai, India (BOM-Chhatrapati Shivaji Intl.)");
		GlobalVariable.LOGGER.log(LogStatus.INFO, "Successfully set text on FlightOrigin text box" );
		click_FirstOption(dr);
	}

	public void click_FirstOption (WebDriver dr)
	{
		try{
			WebDriverWait wait = new WebDriverWait(dr, 5);
			wait.until(ExpectedConditions.elementToBeClickable(DropDwn_FirstOption(dr)));
			//WebElement ele=dr.findElement(By.xpath("//div[@class='multiLineDisplay']"));
			Actions builder=new Actions(dr);
			builder.moveToElement( DropDwn_FirstOption(dr)).click().build().perform();
			//ele.click();
			//CmnKeywrd.moveToElementAndClick(dr.findElement(By.xpath("//div[@class='multiLineDisplay']")));
		}
		catch(Exception e)
		{
			//System.out.println("Exception" +e);
		}
		//dr.findElement(By.xpath()).click();
	}

	public void setText_FlyTo(WebDriver dr)
	{
		txtbox_FlightDestination(dr).sendKeys("Pune, India (PNQ-Lohegaon)");
		click_FirstOption(dr);
	}

	public void select_CurrentDate(WebDriver dr)
	{
		setDate_FlightDate(dr).click();

		try{	
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int year  = localDate.getYear();
			int month = localDate.getMonthValue()-1;
			int day   = localDate.getDayOfMonth();
			//System.out.println(year+month+day);
			String Xpath="//button[@data-year='"+year+"' and  @data-month='"+month+"' and  @data-day='"+day+"'] ";
			WebDriverWait wait = new WebDriverWait(dr, 5);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Xpath)));;
			dr.findElement(By.xpath(Xpath)).click();
		}
		catch(Exception e)
		{
			//System.out.println("Exceppp"+e);
		}
	}


	public void click_SearchBtn(WebDriver dr) throws InterruptedException {

		try{
			Thread.sleep(2000);
			JavascriptExecutor js =(JavascriptExecutor)dr;
			js.executeScript("arguments[0].click();", Btn_Search(dr));
		}
		catch(Exception e)
		{

		}
		//CmnKeywrd.clickObject(dr.findElement(By.xpath("(//span[text()='Search'  and @class='btn-label'])[3]")));
	}

	/*
	public void verifyObjectExist(WebDriver dr) throws InterruptedException {
		//
		Thread.sleep(4000);
		WebElement ele=dr.findElement(By.id("wizard-summary"));
		if(ele.isDisplayed())

			System.out.println("Element present");
			else
			System.out.println("Element not present");	


		//CmnKeywrd.clickObject(dr.findElement(By.xpath("(//span[text()='Search'  and @class='btn-label'])[3]")));
	}*/

	public  String takeSnapShot(WebDriver dr) throws Exception{

		File scrnsht = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		String dest ="D:/QE_Assignment/ScreenShot/"+System.currentTimeMillis()+".png";
		FileUtils.copyFile(scrnsht, new File("D:/QE_Assignment/ScreenShot/"+System.currentTimeMillis()+".png"));
		return dest;
	}

	public static String getScreenshot(WebDriver driver, String screenshotName) throws Exception {
		//below line is just to append the date format with the screenshot name to avoid duplicate names 
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		//after execution, you could see a folder "FailedTestsScreenshots" under src folder
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		//Returns the captured file path
		return destination;
	}

	
	public static String captureScreenShot( WebDriver dr) {
        String imageName = "D:/QE_Assignment/ScreenShot/"+System.currentTimeMillis()+".png";
        File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
        File targetFile = new File("D:/QE_Assignment/ScreenShot/", imageName);
        try {
            FileUtils.copyFile(screenshotFile, targetFile);
        } catch (IOException e) {
           // ExtentReporter.getTest().log(LogStatus.INFO, "Unable to log screenshot");
        }
        return targetFile.getName();
    }

	public void verifyObjectText(WebDriver dr) throws Exception {
		//WebDriverWait wait = new WebDriverWait(dr, 5);
		//wait.until(ExpectedConditions.visibilityOf((WebElement) (By.xpath("(//span[@class='destination'])[1]"))));
		Thread.sleep(5000);
		WebElement ele=dr.findElement(By.id("titleBar"));

		if(ele.isDisplayed())
			try {
				{	
					GlobalVariable.LOGGER.log(LogStatus.PASS, "Successfully verified ticket book titleBar present after ticekt booking"  );
				
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		else
			GlobalVariable.LOGGER.log(LogStatus.FAIL, " verified ticket book titleBar not present after ticekt booking " );



		WebElement ele1=dr.findElement(By.id("flexible-search-toggle"));
		if(ele1.isDisplayed())
			GlobalVariable.LOGGER.log(LogStatus.PASS, "Successfully verified element present  after ticekt booking " );
		else
			GlobalVariable.LOGGER.log(LogStatus.FAIL, " verified verified element not present  after ticekt booking ");



	}
	public  void takeSnapShots(WebDriver dr) throws Exception{

		try{File scrnsht = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		 FileUtils.copyFile(scrnsht, new File("D:/QE_Assignment/ScreenShot/"+System.currentTimeMillis()+".png"));}
		catch(Exception e)
		{
		}

    }

}
