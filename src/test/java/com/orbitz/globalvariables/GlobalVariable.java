package com.orbitz.globalvariables;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class GlobalVariable {
	
	public static final String CHROMEDRIVER="D:\\Shoumen\\Selenium Jars\\geckodriver-v0.20.1-win32\\geckodriver.exe";
	public static final String FIREFOXDRIVER="D:\\Shoumen\\Selenium Jars\\chromedriver_win32\\chromedriver.exe";
	public static final String IEDRIVER="D:\\Shoumen\\Selenium Jars\\IEDriverServer_Win32_3.11.1\\IEDriverServer.exe";
	
	public static ExtentReports REPORT=new ExtentReports("C:\\Rep\\LearnAutomation.html");;
	public static ExtentTest LOGGER;
	
	public static String Browser;
	
}
