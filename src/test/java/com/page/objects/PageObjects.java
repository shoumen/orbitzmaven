package com.page.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.orbitz.com.orbitz.CommonKeywords;
public class PageObjects extends CommonKeywords {

	public  WebElement tab_Flight(WebDriver driver) {

		return (driver.findElement(By.id("tab-flight-tab-hp")));}


	public  WebElement tab_Oneway(WebDriver driver) {
		return (driver.findElement(By.id("flight-type-one-way-label-hp-flight")));
	}

	public  WebElement txtbox_FlightOrigin(WebDriver driver) {
		return (driver.findElement(By.id("flight-origin-hp-flight")));
	}

	public  WebElement txtbox_FlightDestination(WebDriver driver) {
		return (driver.findElement(By.id("flight-destination-hp-flight")));
	}

	public  WebElement Lnk_FlightOriginFistOption(WebDriver driver) {
		return (driver.findElement(By.id("tab-flight-tab-hp")));
	}

	public WebElement setDate_FlightDate(WebDriver driver) {
		return (driver.findElement(By.id("flight-departing-single-hp-flight")));
	}

	public WebElement Btn_Search(WebDriver driver) {
		return (driver.findElement(By.xpath("(//span[text()='Search'  and @class='btn-label'])[3]")));
	}

	public WebElement DropDwn_FirstOption(WebDriver driver) {
		return (driver.findElement(By.xpath("//div[@class='multiLineDisplay']")));


	}


}	 



