package com.orbitz.tc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.orbitz.globalvariables.GlobalVariable;
import com.page.objects.PageObjects;
import com.relevantcodes.extentreports.LogStatus;

  public class OrbitzTc extends PageObjects  {

	public WebDriver driver;
	
	@Parameters("browser")
	@BeforeTest
	public void launchBowser(String browser)
	{
		GlobalVariable.Browser=browser;
		try {
			GlobalVariable.LOGGER=GlobalVariable.REPORT.startTest("TC_01 Verify ticket booked in ORBITZ ");
			if (browser.equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.gecko.driver",GlobalVariable.CHROMEDRIVER);
				driver = new FirefoxDriver();
				GlobalVariable.LOGGER.log(LogStatus.INFO, "Running Test case with " +browser);
			} 
			else if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",GlobalVariable.FIREFOXDRIVER);
				driver = new ChromeDriver();
				GlobalVariable.LOGGER.log(LogStatus.INFO, "Running Test case with " +browser);
			}
			else if (browser.equalsIgnoreCase("IE")) {
				System.setProperty("webdriver.ie.driver",GlobalVariable.IEDRIVER);
				driver = new InternetExplorerDriver();
			}
			driver.get("https://www.orbitz.com/");
			driver.manage().window().maximize();

			//logger=report.startTest("TC_01 Verify ticket booked in ORBITZ ");
			//logger.log(LogStatus.INFO, "Running Test case with " +browser);

		} catch (WebDriverException e) {
			System.out.println(e.getMessage());
		}

	}

	
	@Test
	public void demoTest() throws Exception
	{	
		OrbitzFlow orbitzFlow=new OrbitzFlow();
		orbitzFlow.click_Flight(driver);  
		orbitzFlow.click_OneWay(driver);
		orbitzFlow.setText_Flyfrom(driver);  
		orbitzFlow.setText_FlyTo(driver);
		orbitzFlow.select_CurrentDate(driver);
		orbitzFlow.click_SearchBtn(driver);
		orbitzFlow.verifyObjectText(driver);
		orbitzFlow.takeSnapShot(driver);
		GlobalVariable.REPORT.endTest(GlobalVariable.LOGGER);
		GlobalVariable.REPORT.flush();
	}



}
